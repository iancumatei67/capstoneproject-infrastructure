output "vpc_id" {
  value = aws_vpc.main.id
}

output "alb_security_group" {
  value = aws_security_group.alb_security_group.id
}

output "public_subnets" {
  value = aws_subnet.public_subnet[*].id
}

output "private_subnets" {
  value = aws_subnet.private_subnet[*].id
}

output "aws_security_group_asg" {
  value = aws_security_group.asg_security_group.id
}