#VPC

data "aws_availability_zones" "available" {}


resource "aws_vpc" "main" {
  cidr_block = var.cidr_block_vpc
  enable_dns_support   = true
  tags = {
    Name = "mian-vpc"
    Owner = "miancu"
    Project = "Capstone Project"
  }
}

resource "aws_subnet" "public_subnet" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.public_subnet_cidr[count.index]
  count                   = 2
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  tags = {
    Name = "mian-subnet"
    Owner = "miancu"
    Project = "Capstone Project"
  }
}

resource "aws_subnet" "private_subnet" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.private_subnet_cidr[count.index]
  count             = 2
  availability_zone = data.aws_availability_zones.available.names[count.index]
  tags = {
    Name = "mian-subnet"
    Owner = "miancu"
    Project = "Capstone Project"
  }
}

#Internet Gateway

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main.id
  tags = {
    Name = "mian-igw"
    Owner = "miancu"
    Project = "Capstone Project"
  }
}


#Route table for public subnets

resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
}

resource "aws_route_table_association" "public_route_assoc" {
  count          = 2 
  subnet_id      = aws_subnet.public_subnet[count.index].id
  route_table_id = aws_route_table.public_route_table.id
}



# FIREWALL 

resource "aws_security_group" "alb_security_group" {
  name           = "alb-security-group"
  description    = "ALB Secuity Group"
  vpc_id         = aws_vpc.main.id

  ingress {
    description      = "Allow http from internet"
    protocol         = "tcp"
    from_port        = 80
    to_port          = 80
    cidr_blocks      = ["193.109.103.128/32", "83.142.187.20/32"]
    
  }

  ingress {
    description      = "Allow https request from internet"
    protocol         = "tcp"
    from_port        = 443
    to_port          = 443
    cidr_blocks      = ["193.109.103.128/32", "83.142.187.20/32"]
    
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "asg_security_group" {
  name   = "asg-security-group"
  description = "ASG Security Group"
  vpc_id = aws_vpc.main.id

  ingress {
    description = "Allow http request from Load Balancer"
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    security_groups = [aws_security_group.alb_security_group.id]
  }
   ingress {
    description = "Allow SSH traffic "
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["193.109.103.128/32", "83.142.187.20/32"]  
  }
  
  ingress {
  description = "Allow request to springpetclinic application"
  protocol    = "tcp"
  from_port   = 8888
  to_port     = 8888
  cidr_blocks = ["0.0.0.0/0"]  
}
  ingress {
  description = "Allow request to metrics"
  protocol    = "tcp"
  from_port   = 12345
  to_port     = 12345
  cidr_blocks = ["0.0.0.0/0"]  
}

  ingress {
  description = "Allow MYSQL traffic from RDS instance"
  protocol    = "tcp"
  from_port   = 3306
  to_port     = 3306
  cidr_blocks = ["0.0.0.0/0"]  
}
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
#DATABASE

resource "aws_security_group" "db_sg" {
  name        = "my-db-sg"
  description = "Allow MySQL traffic"
  vpc_id      = aws_vpc.main.id

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
resource "aws_db_subnet_group" "my_db_subnet_group" {
  name       = "my-db-subnet-group"
  subnet_ids = [aws_subnet.public_subnet[0].id, aws_subnet.public_subnet[1].id]
}
resource "aws_db_instance" "my_db" {
  allocated_storage    = 20
  storage_type         = "gp3"
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t3.micro"
  identifier           = "petclinic"
  db_name              = "petclinic"
  username             = "petclinic"
  password             = "petclinic"
  skip_final_snapshot  = true 
  vpc_security_group_ids = [aws_security_group.asg_security_group.id]
  db_subnet_group_name = aws_db_subnet_group.my_db_subnet_group.name
  tags = {
    Name = "petclinic"
    Owner = "miancu"
    Project = "Capstone Project"
  }
}


