# LOAD BALANCER 

resource "aws_lb" "alb" {
  name               = "public-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [var.alb_security_group]
  subnets            = [for i in var.public_subnets : i]

}

resource "aws_lb_target_group" "lb_target_group_app" {
  name     = "lb-target-group-app"
  port     = 8888
  protocol = "HTTP"
  vpc_id   = var.vpc_id

  health_check {
    enabled             = true
    path                = "/"
    protocol            = "HTTP"
    port                = "traffic-port"
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    interval            = 30
  }
}




resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.alb.arn
  port              = 80
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.lb_target_group_app.arn
    }
}



# EC2 INSTANCE AND AUTOSCALING 



resource "aws_launch_template" "launch_template" {
  name          = "aws-launch-template"
  image_id      = var.ami
  instance_type = var.instance_type
  user_data     = var.user_data
  key_name      = var.key_name
  
  network_interfaces { 
    associate_public_ip_address = "true"
    subnet_id           = var.public_subnets[0]
    security_groups     = [var.aws_security_group_asg]
  }

  tag_specifications {
    resource_type = var.resource_type

    tags = {
      Name = "asg-ec2-template"
      Owner = "miancu"
      Project = "Capstone Project"
    }
  }
}

resource "aws_autoscaling_group" "auto_scaling_group" {
  desired_capacity = var.desired_capacity
  max_size         = var.max_size
  min_size         = var.min_size

  target_group_arns   = [aws_lb_target_group.lb_target_group_app.arn]
  vpc_zone_identifier = [for i in var.public_subnets[*] : i]
  name                = "asg-ec2-template"

  launch_template {
    id      = aws_launch_template.launch_template.id
    version = var.vrsion
  }

}
