variable "ami" {}
variable "instance_type" {}
variable "user_data" {}
variable "key_name" {}
variable "resource_type" {}
variable "desired_capacity" {}
variable "max_size" {}
variable "min_size" {}
variable "vrsion" {}
variable "alb_security_group"{}
variable "public_subnets"{}
variable "private_subnets"{}
variable "aws_security_group_asg"{}
variable "vpc_id" {}
