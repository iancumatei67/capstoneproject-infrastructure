#!/bin/bash

if [ ! -d "/tmp/venv" ]; then
    mkdir /tmp/venv
else
    echo "Directory '/tmp/venv' already exists, skipping creation."
fi

python3 -m venv /tmp/venv/ansible_venv
source /tmp/venv/ansible_venv/bin/activate
pip install boto boto3
