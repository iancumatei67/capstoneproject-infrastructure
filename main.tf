terraform {
    required_providers {
        aws = {
            source = "hashicorp/aws"
            version = "5.45.0"
        }
    }
    backend "s3" {
        bucket = "s3terraformtaskstorage"
        region = "us-east-1"
        key    = "storage/statefile"
    }
}


module "networking_module" {
  source                  = "./modules/network"
  cidr_block_vpc          = "10.0.0.0/16"

}


module "ec2_asg_lb_module" {
  source                      = "./modules/ec2_asg_lb"
  ami                         = "ami-02538f8925e3aa27a"
  instance_type               = "t3.small"
  user_data                   = filebase64("metadata_script.sh")
  key_name                    = "terraformkey"
  resource_type               = "instance"
  desired_capacity            = "3"
  max_size                    = "3"
  min_size                    = "3"
  vrsion                      = "$Latest"
  vpc_id                      = module.networking_module.vpc_id
  alb_security_group          = module.networking_module.alb_security_group
  public_subnets              = module.networking_module.public_subnets
  private_subnets             = module.networking_module.private_subnets
  aws_security_group_asg      = module.networking_module.aws_security_group_asg
}
